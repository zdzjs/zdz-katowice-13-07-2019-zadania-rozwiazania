/*Należy stworzyć moduł do już istniejącego serwera WWW (opartego na express), który:
- pozwoli na odbieranie od użytkownika dowolną ilość zdjęć (tablica plików)
- każde zdjęcie będzie zaopatrzone w znak wodny, nazwa strony (dowolna lub 'localhost') 
oraz datę wrzucenia (umiejscowienie znaku w dolnym prawym rogu)
- przeskalować do określonego rozmiaru (z opcją zachowania proporcji lub sztywne wymiary)
- zapisać w bazie SQLITE
- baza powinna pozwalać na zapis opisu zdjęć oraz tworzenia powiązań z galerami

Na podstawie przyjętych danych utowrzyć reklamy na stronę WWW (graficzny)
- z tłem jednolitym, gradientem lub obrazem (przyjmowane jako parametr)
- z napisem, który został przesłany z odpowiednimi parametrami (np. font, pogrubienie, kursywa, podkreślenia, obsługa
minimum 4 rodzajów)
- Tekst powiniem mieć możliwość umiejscowienia dokładnie na środku (zarówno szerokość, jak i wysokość - skalowanie 
wielkości liter)
- zapis do bazy SQLITE



Stworzyć metody pozwalające na wybieranie zdjęć, galerii i/lub reklam do pliku json i przesyłać go
do klienta.*/